function Person(firstName, lastName, gender, isSingle) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.gender = gender;
    this.isSingle = isSingle;
    this.fullName = `${this.firstName} ${this.lastName}`;
    this.greeting = function () {
        console.log(`Hi! My name is ${this.fullName}`);
    };
}

const regie = new Person("Regienald", "Almoite", "Male", true);
regie.greeting();
